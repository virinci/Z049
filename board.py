import random
from tile import Tile, range_map, rect_with_mode, RectMode
from copy import deepcopy as dcp
from shift_board import shift_board
import pygame


class TileBoard:
    def __init__(self, width, height):
        self.gameOver = False
        self.noOfNewValues = 1
        self.noOfValuesAtStart = 2

        self.slideF = 10
        self.hideF = 5
        self.popupF = 10
        self.maxPopSize = 120

        # self.startXY = (20, 20)
        # self.endXY = (580, 580)
        self.startXY = (0, 0)
        self.endXY = (width, height)
        self.rowCol = (4, 4)

        self.dimension = (
            self.endXY[0] - self.startXY[0],
            self.endXY[1] - self.startXY[1],
        )

        # paddings
        self.innerPadding = 20
        self.outerPadding = 10

        # val for blank
        self.blank = 0

        # this means backend of tileboard
        eachRow = [self.blank for i in range(self.rowCol[1])]
        self.backBoard = [dcp(eachRow) for j in range(self.rowCol[0])]

        # calculation of height, width, radius of each tile
        paddingSpaceX = (2 * self.outerPadding) + (
            (self.rowCol[1] + 1) * self.innerPadding
        )
        paddingSpaceY = (2 * self.outerPadding) + (
            (self.rowCol[0] + 1) * self.innerPadding
        )
        eachTileW = (self.dimension[0] - paddingSpaceX) / self.rowCol[1]
        eachTileH = (self.dimension[1] - paddingSpaceY) / self.rowCol[0]
        self.eachTileH = eachTileH
        self.eachTileW = eachTileW
        self.eachTileR = 10

        # self.dirLog = []
        self.currDir = (0, 0)
        # if animation is going on
        self.animation = False

        self.posOfTiles = []
        self.tileObjects = []

        for i in range(self.rowCol[0]):
            for j in range(self.rowCol[1]):
                self.posOfTiles.append(self.giveMeXY((i, j)))

        self.maxFontSize = self.eachTileH // 2
        self.minFontSize = 12

    # updates backend of board with current values of each tiles

    def updateBackBoard(self):
        for i in range(len(self.tileObjects)):
            twoDInd = (i // self.rowCol[0], i % self.rowCol[1])
            self.backBoard[twoDInd[0]][twoDInd[1]] = self.tileObjects[i].val

    # creates tiles for each col of each row

    def createTiles(self):
        for i in range(self.rowCol[0]):
            for j in range(self.rowCol[1]):
                newTile = Tile(self, (i, j), self.blank)
                self.tileObjects.append(newTile)

    # gives (x, y) location of center of tile at ind index

    def giveMeXY(self, ind):
        # row
        i = ind[0]
        # col
        j = ind[1]
        posX = (
            self.startXY[0]
            + self.outerPadding
            + ((j + 1) * self.innerPadding)
            + ((j + 0.5) * self.eachTileW)
        )
        posY = (
            self.startXY[1]
            + self.outerPadding
            + ((i + 1) * self.innerPadding)
            + ((i + 0.5) * self.eachTileH)
        )
        posXY = (posX, posY)

        return posXY

    def ifGameOver(self, initBoard, finalBoard):
        return not any(tile == self.blank for row in finalBoard for tile in row)

    ############################################################################
    ############################################################################
    ############################################################################
    ############################################################################
    """
    this block shows to the screen
    """

    def drawTileBoard(self, surface: pygame.Surface):
        color = (187, 173, 160, 255)
        rect = pygame.Rect(
            self.startXY[0], self.startXY[1], self.dimension[0], self.dimension[1]
        )
        pygame.draw.rect(surface, color, rect)

    def drawAllTiles(self, surface: pygame.Surface):
        # shows all available tiles in tileObjects list
        for i in self.tileObjects:
            i.drawTile(surface)

    def drawBackgroundTiles(self, surface: pygame.Surface):
        """Show blank spaces."""
        color = (200, 188, 176, 255)
        for i in self.posOfTiles:
            rect = rect_with_mode(
                i[0], i[1], self.eachTileW, self.eachTileH, RectMode.CENTER
            )
            pygame.draw.rect(surface, color, rect, border_radius=self.eachTileR)

    def drawTiles(self, surface: pygame.Surface):
        if self.animation or self.currDir != (0, 0):
            # Start or continue animating tiles.
            self.animate(surface)

            if self.ifDoneAnimation():
                self.animation = False
                self.currDir = (0, 0)
                self.updateBackBoard()
                self.drawAllTiles(surface)
        else:
            self.drawAllTiles(surface)

    ############################################################################
    ############################################################################
    ############################################################################
    ############################################################################
    """
    this block is for adding values to tiles
    """

    # adds two values to two random tiles
    # often used at the beginning of game
    def initTileAdd(self):
        # runs ranTileAdd function n times
        for i in range(self.noOfValuesAtStart):
            # toBeginWith = 2
            _ranVal, _ranInd = self.ranTileAdd()
            for eachTile in self.tileObjects:
                if eachTile.pos == _ranInd:
                    eachTile.val = _ranVal
            self.updateBackBoard()

    # function generates random value and position

    def ranTileAdd(self):
        # random value
        # probability out of 10
        probabilityDist = {
            2: 9,
            4: 1,
            # self.blank : 1
        }

        tenLs = []
        for i in probabilityDist:
            tenLs.extend([i] * probabilityDist[i])

        ranVal = random.choice(tenLs)

        # random index
        blankIndices = []
        for row in range(self.rowCol[0]):
            for col in range(self.rowCol[1]):
                if self.backBoard[row][col] == self.blank:
                    blankIndices.append((row, col))

        ranInd = random.choice(blankIndices)

        return ranVal, ranInd

    # is supposed to add random value after each movement

    def addRanTile(self):
        _ranVal, _ranInd = self.ranTileAdd()
        if _ranVal == self.blank:
            return
        for eachTile in self.tileObjects:
            if eachTile.pos == _ranInd:
                eachTile.val = _ranVal
                break
        self.fusionList.append(_ranInd)

    ############################################################################
    ############################################################################
    ############################################################################
    ############################################################################
    """
    this block is all for animation
    animation doc :
    stage 0 :
        checks if no movement
            ends animation
    stage 1 : sliding
        background
        board
        bg tiles
        static tiles / which didn't move
        sliding tiles / position constantly updated per frame
    stage 2 : popping off fused tiles
        background
        board
        bg tiles
        static tiles
        moved tiles which didn't fuse
        # fused tiles aren't showed at all
    stage 3 : popping up fused tiles and random tile simultaneously
        background
        board
        bg tiles
        static tiles
        moved tiles which didn't fuse
        new tiles / size is constantly updated per frame

    """

    def animate(self, surface: pygame.Surface):
        assert self.currDir != (0, 0)

        # If not already animating, then start it.
        if not self.animation:
            # Setup for starting animation
            self.animation = True
            self.slideFrames = [0, self.slideF]
            self.popOffFrames = [0, self.hideF]
            self.popUpFrames = [0, self.popupF]
            self.ranTileAdded = False
            self.updatedAfterSlide = False
            self.movement = True

            # values of interest for animation
            self.finalRes = shift_board(self.backBoard, self.currDir)
            self.fullLog = self.giveFullLog(self.currDir)
            self.detailedFusionList = self.whichFuses(self.fullLog)
            self.fusionList = []
            for i in self.detailedFusionList:
                if i[-1] not in self.fusionList:
                    self.fusionList.append(i[-1])

            if self.finalRes == self.backBoard:
                self.movement = False

            if self.ifGameOver(self.backBoard, self.finalRes):
                self.gameOver = True

        if self.gameOver:
            return

        # Checks if no movement occured and if so, stops animation.
        if not self.movement:
            self.slideFrames[0] = self.slideFrames[1] * 2
            self.popUpFrames[0] = self.popUpFrames[1] * 2
            self.popOffFrames[0] = self.popOffFrames[1] * 2
            self.animation = False
            self.currDir = (0, 0)
            self.ranTileAdded = True
            self.updatedAfterSlide = True
            return

        # Looping animation
        if not self.ifDoneSliding(self.slideFrames):
            self.boardSlide(self.slideFrames, surface)
            self.slideFrames[0] += 1
        elif not self.updatedAfterSlide:
            self.updateAfterSlide()
            self.updatedAfterSlide = True
            self.updateBackBoard()
        elif not self.ifDonePoppingOff(self.popOffFrames):
            self.poppedOff(self.popOffFrames, surface)
            self.popOffFrames[0] += 1
        elif not self.ifDonePoppingUp(self.popUpFrames):
            # adds random tile if some movement occured
            if not self.ranTileAdded and self.movement:
                for i in range(self.noOfNewValues):
                    self.addRanTile()
                    self.ranTileAdded = True
                    self.updateBackBoard()
            self.poppedUp(self.popUpFrames, surface)
            self.popUpFrames[0] += 1

    # updates x, y of tiles per frame to create sliding effect

    def boardSlide(self, _sFrames, surface: pygame.Surface):
        # static tiles, which didn't move
        for eachLog in self.fullLog:
            # eliminating blanks
            if eachLog[-1] == (-1, -1):
                continue

            if eachLog[0] == eachLog[-1] or eachLog[-1] == (-1, -1):
                for eachTile in self.tileObjects:
                    if eachTile.pos == eachLog[0]:
                        eachTile.drawTile(surface)
                        break
        # sliding tiles
        for eachLog in self.fullLog:
            if eachLog[0] != eachLog[-1] and eachLog[-1] != (-1, -1):
                startInd = eachLog[0]
                endInd = eachLog[-1]
                startXY = self.giveMeXY(startInd)
                endXY = self.giveMeXY(endInd)

                newX = range_map(_sFrames[0], 0, _sFrames[1], startXY[0], endXY[0])
                newY = range_map(_sFrames[0], 0, _sFrames[1], startXY[1], endXY[1])

                for eachTile in self.tileObjects:
                    if eachTile.pos == startInd:
                        eachTile.xy = (newX, newY)
                        eachTile.drawTile(surface)
                        if not self.movement:
                            self.movement = True

    # genuinly changes values of all tiles with result
    # restores x, y of tiles to supposed position

    def updateAfterSlide(self):
        for i in range(len(self.tileObjects)):
            twoDInd = (i // self.rowCol[0], i % self.rowCol[1])
            self.tileObjects[i].val = self.finalRes[twoDInd[0]][twoDInd[1]]
            self.tileObjects[i].xy = self.giveMeXY(twoDInd)

    # hides fused tiles

    def poppedOff(self, _pFrames, surface: pygame.Surface):
        for eachTile in self.tileObjects:
            if eachTile.pos not in self.fusionList:
                eachTile.drawTile(surface)

    # pops up random and fused tiles on board

    def poppedUp(self, _pFrames, surface: pygame.Surface):
        for eachTile in self.tileObjects:
            if eachTile not in self.fusionList:
                eachTile.drawTile(surface)
        for eachTile in self.tileObjects:
            if eachTile.pos in self.fusionList:
                if _pFrames[0] >= _pFrames[1]:
                    eachTile.h = self.eachTileH
                    eachTile.w = self.eachTileW
                else:
                    thisSize = range_map(
                        _pFrames[0], 0, _pFrames[1], 0, self.maxPopSize
                    )
                    eachTile.h = self.eachTileH * (thisSize / 100)
                    eachTile.w = self.eachTileW * (thisSize / 100)
                eachTile.drawTile(surface)

    """
    Checks for each stage of animation
    """

    def ifDonePoppingUp(self, _pFrames):
        return _pFrames[0] > _pFrames[1]

    def ifDonePoppingOff(self, _pFrames):
        return _pFrames[0] > _pFrames[1]

    def ifDoneSliding(self, _sFrames):
        return _sFrames[0] > _sFrames[1]

    def ifDoneAnimation(self):
        if self.animation == False:
            return True
        if self.popOffFrames[0] <= self.popOffFrames[1]:
            return False
        if self.popUpFrames[0] <= self.popUpFrames[1]:
            return False
        if self.slideFrames[0] <= self.slideFrames[1]:
            return False
        if self.updateAfterSlide == False:
            return False
        return True

        """
        if self.animation is False:
            return True
        return not (
            self.popOffFrames[0] <= self.popOffFrames[1]
            or self.popUpFrames[0] <= self.popUpFrames[1]
            or self.slideFrames[0] <= self.slideFrames[1]
            or self.updatedAfterSlide is False
        )
        """

    ############################################################################

    """
    # block below is a good example of necessary trash
    this block is for some background info and logs about movement of tiles
    is important for animation

    purpose :
        function
        giveFullLog : [[(x1, y1), (a, b), steps, (x2, y2)],
                        ...]
            (x1, y1) is initial index of tile
            (a, b) is direction
            steps is no of tiles this tile has to cross
            (x2, y2) is final index of tile
        whichFuses : [[(x1, y1), (x2, y2), (x, y)],
                        ...]
            (x1, y1) is initial index of some tile
            (x2, y2) is initial index of some other tile
            (x, y) is common final index of both tiles

        rest all functions are necessary for working of
        above functions
    """

    def whichFuses(self, _finalStepsLog):
        """this function returns list of list of init pos ind
        and final pos ind"""
        noBlankStepsLog = []
        for i in _finalStepsLog:
            if i[2] != -1:
                noBlankStepsLog.append(i)

        # _steps is of form initpos, direction, steps, finalpos
        # var contains initpos of value which fused
        sameDestination = []
        for i in range(len(noBlankStepsLog)):
            for j in range(len(noBlankStepsLog)):
                if i != j:
                    # checking for same finalpos
                    if noBlankStepsLog[i][-1] == noBlankStepsLog[j][-1]:
                        sameDestination.append(
                            [
                                noBlankStepsLog[i][0],
                                noBlankStepsLog[j][0],
                                noBlankStepsLog[i][-1],
                            ]
                        )

        return sameDestination

    def giveFullLog(self, direction):
        stepsLog = []

        if direction == (-1, 0):
            # for i in range(self.rowCol[0]):
            for i in range(len(self.backBoard)):
                eachRow = self.backBoard[i]
                for j in range(len(eachRow)):
                    stepsTaken = self.giveNoOfStepsTaken(eachRow, j)
                    stepsLog.append([(i, j), direction, stepsTaken])

        elif direction == (1, 0):
            for i in range(len(self.backBoard)):
                eachRow = self.backBoard[i]
                eachRow = eachRow[::-1]
                ln = len(eachRow)
                for j in range(len(eachRow)):
                    stepsTaken = self.giveNoOfStepsTaken(eachRow, j)
                    stepsLog.append([(i, ln - j - 1), direction, stepsTaken])

        elif direction == (0, 1):
            # first traverses through each column
            for i in range(len(self.backBoard[0])):
                eachCol = []
                for j in self.backBoard:
                    eachCol.append(j[i])
                for j in range(len(eachCol)):
                    stepsTaken = self.giveNoOfStepsTaken(eachCol, j)
                    stepsLog.append([(j, i), direction, stepsTaken])

        elif direction == (0, -1):
            # first traverses through each column
            for i in range(len(self.backBoard[0])):
                eachCol = []
                for j in self.backBoard:
                    eachCol.append(j[i])

                eachCol = eachCol[::-1]
                ln = len(eachCol)
                for j in range(len(eachCol)):
                    stepsTaken = self.giveNoOfStepsTaken(eachCol, j)
                    stepsLog.append([(ln - j - 1, i), direction, stepsTaken])

        finalStepsLog = self.addFinalInd(stepsLog)

        return finalStepsLog

    def giveNoOfFuseBefore(self, ls, ind):
        noOfFuseBefore = 0
        howManyFuse = 2

        allValuesTill = ls[: ind + 1]
        allValuesTill.append(1)
        splitGroups = []
        nonBlankValuesTill = []

        for i in allValuesTill:
            if i != self.blank:
                nonBlankValuesTill.append(i)

        for i, j in enumerate(nonBlankValuesTill):
            if i == 0:
                tempValue = j
                logInd = i
            else:
                if tempValue != j:
                    splitGroups.append(i - logInd)
                    logInd = i
                    tempValue = j

        for i in splitGroups:
            noOfFuseBefore += i // howManyFuse

        return noOfFuseBefore

    def giveNoOfBlankBefore(self, ls, ind):
        return sum(ls[i] == self.blank for i in range(ind))

    def giveNoOfStepsTaken(self, ls, ind):
        if ls[ind] == self.blank:
            return -1
        blank = self.giveNoOfBlankBefore(ls, ind)
        fuse = self.giveNoOfFuseBefore(ls, ind)
        return blank + fuse

    def addFinalInd(self, _stepsLog):
        """this function appends to the log the final position ind"""
        # _stepsLog of form indices, direction, steps

        _steps = dcp(_stepsLog)
        _stepsIter = dcp(_stepsLog)

        for ind, valLs in enumerate(_stepsIter):
            i = valLs[0]
            j = valLs[1]
            k = valLs[2]

            if k == -1:
                finalXY = (-1, -1)
                _steps[ind].append(finalXY)
                continue

            if j == (-1, 0):
                finalX = i[0]
                finalY = i[1] - k
                finalXY = (finalX, finalY)
            elif j == (1, 0):
                finalX = i[0]
                finalY = i[1] + k
                finalXY = (finalX, finalY)
            elif j == (0, 1):
                finalX = i[0] - k
                finalY = i[1]
                finalXY = (finalX, finalY)
            elif j == (0, -1):
                finalX = i[0] + k
                finalY = i[1]
                finalXY = (finalX, finalY)
            else:
                assert False, "unreachable"

            _steps[ind].append(finalXY)

        return _steps
