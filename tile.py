import pygame
from enum import auto, Enum


pygame.font.init()
font_path = "JetBrainsMono-Medium.ttf"
font_size = 48
font = pygame.font.Font(font_path, font_size)


class RectMode(Enum):
    CORNER = auto()
    CORNERS = auto()
    CENTER = auto()
    RADIUS = auto()


def rect_with_mode(
    a: int, b: int, c: int, d: int, mode: RectMode = RectMode.CORNER
) -> pygame.Rect:
    match mode:
        case RectMode.CORNER:
            return pygame.Rect(a, b, c, d)
        case RectMode.CORNERS:
            x = min(a, c)
            y = min(b, d)
            width = abs(a - c) + 1
            height = abs(b - d) + 1
            return pygame.Rect(x, y, width, height)
        case RectMode.CENTER:
            rect = pygame.Rect(a, b, c, d)
            rect.size = (c, d)
            rect.center = (a, b)
            return rect
        case RectMode.RADIUS:
            return rect_with_mode(a, b, 2 * c, 2 * d, RectMode.CENTER)


def range_map(s, a1, a2, b1, b2):
    return b1 + (s - a1) * (b2 - b1) // (a2 - a1)


class Tile:
    colors = {
        "darkText": (119, 110, 101, 255),
        "lightText": (249, 246, 242, 255),
        0: (200, 188, 176, 255),
        2: (238, 228, 218, 255),
        4: (237, 224, 200, 255),
        8: (242, 177, 121, 255),
        16: (245, 149, 99, 255),
        32: (246, 124, 95, 255),
        64: (246, 94, 59, 255),
        128: (241, 204, 112, 255),
        256: (244, 200, 94, 255),
        512: (243, 195, 78, 255),
        1024: (242, 191, 57, 255),
        2048: (247, 188, 42, 255),
        4096: (255, 79, 60, 255),
        8192: (255, 53, 28, 255),
        16384: (255, 51, 30, 255),
    }

    def __init__(self, board, pos, val):
        self.pos = pos
        self.val = val

        # tile dimensions
        self.h = board.eachTileH
        self.w = board.eachTileW
        self.r = board.eachTileR

        self.board = board

        # x and y of the center of the tile.
        self.xy = board.giveMeXY(pos)

    def drawTile(self, surface: pygame.Surface):
        # selecting color and mode
        col = (
            Tile.colors[0]
            if self.val == self.board.blank
            else Tile.colors.get(self.val, Tile.colors[16384])
        )

        # rectMode(CENTER)
        color = (col[0], col[1], col[2], col[3])
        rect = rect_with_mode(self.xy[0], self.xy[1], self.w, self.h, RectMode.CENTER)
        pygame.draw.rect(surface, color, rect, border_radius=self.r)

        # no text if tile is blank
        if self.val == self.board.blank:
            return

        eachTileH = self.board.eachTileH
        eachTileW = self.board.eachTileW

        # to avoid awkward text when tile is popping up during animation
        if self.w > eachTileW / 2 and self.h > eachTileH / 2:
            # choosing appropriate fontsize for different texts lengths
            ln = len(str(self.val))
            fontsize = int(
                range_map(ln, 1, 10, self.board.maxFontSize, self.board.minFontSize)
            )

            color = (
                Tile.colors["darkText"] if self.val <= 4 else Tile.colors["lightText"]
            )

            """
            rectMode(CENTER)
            textSize(fontsize)
            textAlign(CENTER)

            text(self.val, self.xy[0], self.xy[1] + (fontsize / 3))
            """

            text_surface = font.render(str(self.val), True, color)
            rect = text_surface.get_rect(center=self.xy)
            surface.blit(text_surface, rect)
