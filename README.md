# Z049
A low quality 2048 game with much less thought put into it.

## To-Do
- [x] Fix blank screen on game over
- [x] Rewrite it using PyGame
- [ ] Use a smaller font size when the tile numbers get large

## Changelog
- 2023-09-30: Rewrite it in PyGame
- 2022-08-20: Modified the project structure
- 2020-03-29: Init and finish a working game
