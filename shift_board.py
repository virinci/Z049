def fuse_line(tile_line: list[int]) -> list[int]:
    """Shift the line towards the beginning while merging the equal tiles."""
    tile_line.append(-1)
    fused_line = []
    skip_next = False

    for x, y in zip(tile_line, tile_line[1:]):
        if skip_next:
            skip_next = False
            continue

        if x == y:
            fused_line.append(x + y)
            skip_next = True
        else:
            fused_line.append(x)

    return fused_line


def retain_relevant_tiles(tile_line: list[int]) -> list[int]:
    """Retain only the non-zero tiles."""
    return [tile_value for tile_value in tile_line if tile_value != 0]


def shift_line(tile_line: list[int]) -> list[int]:
    """Shift line and return the shifted line."""
    length = len(tile_line)
    new_line = fuse_line(retain_relevant_tiles(tile_line))
    new_line.extend([0] * (length - len(new_line)))
    return new_line


def shift_board(board: list[list[int]], direction: tuple[int, int]) -> list[list[int]]:
    """Shift the board in the given direction."""
    rows, columns = len(board), len(board[0])
    shifted_board = [[0] * columns for _ in range(rows)]

    if direction == (-1, 0):
        for y in range(rows):
            line = board[y]
            new_line = shift_line(line)
            for x in range(columns):
                shifted_board[y][x] = new_line[x]

    elif direction == (1, 0):
        for y in range(rows):
            line = board[y][::-1]
            new_line = shift_line(line)
            for x in range(columns):
                shifted_board[y][x] = new_line[columns - x - 1]

    elif direction == (0, 1):
        for x in range(columns):
            line = [board[y][x] for y in range(rows)]
            new_line = shift_line(line)
            for y in range(rows):
                shifted_board[y][x] = new_line[y]

    elif direction == (0, -1):
        for x in range(columns):
            line = [board[y][x] for y in range(rows)][::-1]
            new_line = shift_line(line)
            for y in range(rows):
                shifted_board[y][x] = new_line[rows - y - 1]

    return shifted_board
