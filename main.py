import pygame
from board import TileBoard


SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
TITLE = "Z049 - DEBUG"
WINDOW_COLOR = (100, 100, 100)


def tileboard_init() -> TileBoard:
    board = TileBoard(SCREEN_WIDTH, SCREEN_HEIGHT)
    board.createTiles()
    board.updateBackBoard()
    board.initTileAdd()

    # Game controls
    # board.slideF = 30
    # board.hideF = 10
    # board.popupF = 20
    # board.maxPopSize = 150
    # board.noOfValuesAtStart = 4
    # board.noOfNewValues = 2

    # note : control padding setting from within tileBoard class
    # as board object has already been created and some settings
    # are already calculated
    # board.innerPadding = 10
    # board.outerPadding = 10

    return board


pygame.init()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption(TITLE)

running = True
clock = pygame.time.Clock()

board = tileboard_init()

font_path = "JetBrainsMono-Medium.ttf"
font_size = 18
font = pygame.font.Font(font_path, font_size)

# Drawing FPS on the window.
fps_bg = pygame.Surface((25, 25))
fps_bg.fill((255, 255, 255))


def draw_fps(surface: pygame.Surface):
    fps_text = str(int(clock.get_fps()))
    fps_surface = font.render(fps_text, True, pygame.Color("black"))
    surface.blit(fps_bg, (0, 0))
    surface.blit(fps_surface, (0, 0))


while running:
    for ev in pygame.event.get():
        match ev.type:
            case pygame.QUIT:
                board.gameOver = True
                running = False
                break
            case pygame.KEYDOWN:
                if ev.key == pygame.K_r:
                    board = tileboard_init()
                elif not board.animation:
                    match ev.key:
                        case pygame.K_w | pygame.K_UP:
                            board.currDir = (0, 1)
                        case pygame.K_s | pygame.K_DOWN:
                            board.currDir = (0, -1)
                        case pygame.K_a | pygame.K_LEFT:
                            board.currDir = (-1, 0)
                        case pygame.K_d | pygame.K_RIGHT:
                            board.currDir = (1, 0)

    screen.fill(WINDOW_COLOR)

    # Game States:
    #   Idle
    #   Key Pressed -> Start Animation
    #   Animating
    #   Game Over
    board.drawTileBoard(screen)
    board.drawBackgroundTiles(screen)

    if board.gameOver:
        board.drawAllTiles(screen)
    else:
        board.drawTiles(screen)

    draw_fps(screen)

    clock.tick(60)
    pygame.display.flip()
